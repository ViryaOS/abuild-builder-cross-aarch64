## FAQs

### Why is there an RSA Private Key in `docker-extras` directory?

`abuild` command does not offer a clean way of separating package building from
package signing. Therefore we need to have a RSA private key in the container.

Upstream Alpine Linux is working on fixing this issue.

https://lists.alpinelinux.org/alpine-devel/6057.html

See section - _separate out signing process of packages and index_

At this time, you should not rely on the integrity and authenticity
assertions of builder RSA Public Key.

### Why do we need `abuild-add-alpine-v3-7-main.patch`?

In case of abuild-builder the (hypothetical) sysroot-x86\_64 and chroot are one
and the same.

However for abuild-builder-cross-aarch64 the sysroot-aarch64 and chroot are
different. Unlike the chroot, we do not maintain repository information in
sysroot-aarch64.

```
8dd26232935d:~$ pwd
/home/builder

8dd26232935d:~$ cat /etc/apk/repositories
http://dl-cdn.alpinelinux.org/alpine/v3.7/main
http://dl-cdn.alpinelinux.org/alpine/v3.7/community

8dd26232935d:~$ cat sysroot-aarch64/apk/repositories
cat: can't open 'sysroot-aarch64/apk/repositories': No such file or directory
```

Therefore we add `--repository http://dl-cdn.alpinelinux.org/alpine/v3.7/main`
so when apk runs with `--root sysroot-aarch64` it can find the repository from
where it can pull aarch64 packages.
